<g:render template="/index/head"/>
<div class="row">
    <div class="column small-12 small-centered">
        <div class=" ss-callout column small-6 small-centered">
            <div class="callout success large">
                <h5>Спасибо за участие!</h5>

                <p>Имя того кому тебе нужно подарить подарок, будет выслано тебе на почту:)</p>

                <p>Рассылку сделаем когда все впишут себя и свою почту на сайте</p>
            </div>
        </div>
    </div>
</div>
<g:render template="/index/footer"/>
