<g:render template="/index/head"/>

<body>
<div class="row">
    <div class="column small-6 small-centered">
        <form class="ss-form" action="<g:createLink controller="index" action="saveUser"/>">
            <div class="row">
                <div class="row">
                    <div class="col-sm-12 text-center">
                        <h3>Кто ты?</h3>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-12">
                    <label>Имя
                        <input type="text" name="name" id="name" placeholder="Имя">
                    </label>
                </div>
            </div>

            <div class="row">

                <div class="col-sm-12">
                    <label>Email
                        <input type="email" name="email" id="email" placeholder="Email">
                    </label>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <button type="submit" class="alert hollow expanded button ss-submit-btn">Отправить</button>
                </div>
            </div>
        </form>
    </div>
</div>
<g:render template="/index/footer"/>
