$(".ss-submit-btn").click(function (e) {
    e.preventDefault();

    var emailInput = $('#email');
    var nameInput = $('#name');

    var userName = nameInput.val();
    var userEmail = emailInput.val();
    var proceed = true;

    if (userName == "") {
        nameInput.addClass('error');
        proceed = false;
    }
    if (!validateEmail(userEmail)) {
        emailInput.addClass('error');
        proceed = false;
    }

    if (proceed) {
        $('.ss-form').submit();
    }
});

$('#name, #email').focus(function () {
    $(this).removeClass('error');
});


function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}
