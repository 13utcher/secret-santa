package secret.santa

class IndexController {

    def emailService

    def index() {
    }

    def saveUser() {
        User user = User.findByEmail(params.email)
        if (!user) {
            user = new User(params)
            user.save()
        }
        redirect uri: '/success'
    }

    def success() {}

    def sendEmail() {
        List<User> users = User.getAll()
        Map<User, User> pairs = SantaUtils.getSantaPairs(users)
        pairs.each { user, opponent ->
            emailService.sendEmail(user, opponent)
        }
        render 'ok'
    }
}
