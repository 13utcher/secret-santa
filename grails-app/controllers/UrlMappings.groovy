class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?" {
            constraints {
                // apply constraints here
            }
        }

        "/"(controller: 'index', view: "/index")
        "/success"(controller: 'index', action: "success")
        "500"(view: '/error')
        "404"(view: '/notFound')
    }
}
