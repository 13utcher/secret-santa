package secret.santa

import grails.transaction.Transactional

@Transactional
class EmailService {

    def mailService

    def sendEmail(User recipient, User opponent) {
        mailService.sendMail {
            to recipient.email
            subject "You Secret Santa opponent"
            text "Ты даришь подарок ${opponent.name}"
        }
    }
}
