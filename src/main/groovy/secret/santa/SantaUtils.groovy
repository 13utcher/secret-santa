package secret.santa

/**
 * Created by 13utcher on 20.12.2015.
 */
class SantaUtils {

    static Random random = new Random()

    static Map<User, User> getSantaPairs(List<User> users) {
        Map<User, User> resultMap = [:]
        List<User> attendees = users.collect()
        List<User> opponents = users.collect()
        Iterator<User> userator = attendees.iterator()
        while (userator.hasNext()) {
            User currentUser = userator.next()
            User opponent = opponents.get(random.nextInt(opponents.size()))
            resultMap.put(currentUser, opponent)
            userator.remove()
            opponents.remove(opponent)
        }
        if (isValidPairs(resultMap)) {
            resultMap
        } else {
            getSantaPairs(users)
        }

    }

    static boolean isValidPairs(Map<User, User> pairs) {
        boolean result = true
        for (User currentUser : pairs.keySet()) {
            User opponent = pairs.get(currentUser)
            if (currentUser.equals(opponent)) {
                result = false
                break
            }
        }
        result
    }
}
